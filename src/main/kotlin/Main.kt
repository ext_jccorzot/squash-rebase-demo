fun main(args: Array<String>) {
    println("Hello commit one")
    println("Hello commit two")
    println("Hello feature three")

    println("Hello feature two")

    // Try adding program arguments via Run/Debug configuration.
    // Learn more about running applications: https://www.jetbrains.com/help/idea/running-applications.html.
    println("Program arguments: ${args.joinToString()}")
}